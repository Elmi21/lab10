#include <pthread.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <wait.h> 
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#define MAX_SIZE_STRING 25
#define CONST_TWO 2
#define CONST_ONE 1
void *calc_expected_value(void* arg)
{
    int* fp = (int*) arg;
    int num_elem;
    float *expected_value = (double*) malloc(sizeof (double));
    *expected_value = 0;
    fscanf(fp, "%d", &num_elem);
    printf("Количество элементов в массиве данных: %d\n", num_elem);
    float elems[num_elem];
    for (int i = 0; i < num_elem * CONST_TWO; i = i + CONST_TWO) {
        fscanf(fp, "%f", &elems[i]);
        fscanf(fp, "%f", &elems[i + CONST_ONE]);
        *expected_value += elems[i] * elems[i + CONST_ONE];
    }
    printf("Потомок: математическое ожидание равно: %f\n", *expected_value);
    pthread_exit((void*) expected_value);
}

int main(int argc, char * argv[])
{
    int num_files, result;
    printf("Введите количество файлов\n");
    scanf("%d", &num_files);
    int fp_mas[num_files];
    pid_t PidsProcess[num_files];
    pthread_t threads[num_files];
    char name_files[num_files][CONST_TWO];
    for (int i = 0; i < num_files; i++) {
        sprintf(name_files[i], "%d", i + CONST_ONE);
        if ((fp_mas[i] = fopen(name_files[i], "r")) == NULL) {
            printf("Ошибка при открытии файла!\n");
            exit(1);
        }
    }
    void *status[num_files];
    for (int i = 0; i < num_files; i++) {
        result = pthread_create(&threads[i], NULL, calc_expected_value, fp_mas[i]);
        printf("Результат создания потока=%d\n", result);
        if (result != 0) {
            printf("Ошибка в создании потока!\n");
            exit(1);
        }
    }
    for (int i = 0; i < num_files; i++) {
        result = pthread_join(threads[i], &status[i]);
        if (result != 0) {
            perror("Ошибка в ожидании неотсоединённого процесса\n");
            return EXIT_FAILURE;
        } else {
            printf("Родитель: математическое ожидание[%d]=%f\n", i, *((float*) status[i]));
        }
        free(status[i]);
    }
    return 0;
}